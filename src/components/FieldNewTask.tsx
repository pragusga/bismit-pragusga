import { FC, useState } from 'react';
import { BsCardList, BsCalendar } from 'react-icons/bs';
import {
  StyledBtnSubmit,
  InputActivity,
  IconStyled,
  InputDate,
  TitleForm,
  TextForm,
} from './StyledComponents';

interface NewTaskProps {
  inputData: (titleTask: string, deadlineDate: string) => void;
}

export const FieldNewTask: FC<NewTaskProps> = ({ inputData }): JSX.Element => {
  let currentDate = new Date().getDate().toString();
  if (Number(currentDate) < 10) currentDate = '0' + currentDate;
  let currentMonth = (new Date().getMonth() + 1).toString();
  if (Number(currentMonth) < 10) currentMonth = '0' + currentMonth;
  const currentYear = new Date().getFullYear();
  const currentTime = currentYear + '-' + currentMonth + '-' + currentDate;
  const [selectedDate, setSelectedDate] = useState<string | null>(null);
  const [title, setTitle] = useState<string | null>(null);

  const handleSubmit = () => {
    title && selectedDate && inputData(title, selectedDate);
  };

  return (
    <>
      <div className="form">
        <TitleForm>
          <TextForm>Task Name</TextForm>
          <IconStyled>
            <BsCardList />
          </IconStyled>
        </TitleForm>
        <div className="task-name">
          <InputActivity
            type="text"
            placeholder="New Activity"
            onChange={(e) => setTitle(e.target.value)}
            size={40}
            required
          />
        </div>
      </div>
      <div className="form">
        <TitleForm>
          <TextForm>Due Date</TextForm>
          <IconStyled>
            <BsCalendar />
          </IconStyled>
        </TitleForm>
        <div className="form-date">
          <InputDate
            type="date"
            onChange={(e) => setSelectedDate(e.target.value)}
            min={currentTime}
            required
          />
        </div>
      </div>
      <StyledBtnSubmit onClick={handleSubmit}>
        <span style={{ color: '#13293d' }}>+</span> Add Task
      </StyledBtnSubmit>
    </>
  );
};
