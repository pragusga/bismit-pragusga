import { FC } from 'react';

import { StyledTextBox } from './StyledComponents';

export const TextBox: FC = ({ children }): JSX.Element => {
  return <StyledTextBox>{children}</StyledTextBox>;
};
