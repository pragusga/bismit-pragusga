import { FC } from 'react';
import { BsTrash } from 'react-icons/bs';

import { Todo, TodoDeadline, TrashIcon, TodoTitle } from './StyledComponents';

const generateDate = (fullDate: String): String => {
  let [year, month, date] = fullDate.split('-');
  switch (month) {
    case '01':
      month = 'January';
      break;
    case '02':
      month = 'February';
      break;
    case '03':
      month = 'March';
      break;
    case '04':
      month = 'April';
      break;
    case '05':
      month = 'May';
      break;
    case '06':
      month = 'June';
      break;
    case '07':
      month = 'July';
      break;
    case '08':
      month = 'August';
      break;
    case '09':
      month = 'September';
      break;
    case '10':
      month = 'October';
      break;
    case '11':
      month = 'November';
      break;
    case '12':
      month = 'December';
      break;
  }

  if (date === '01' || date === '21' || date === '31') {
    date += 'st';
  } else if (date === '02' || date === '22') {
    date += 'nd';
  } else if (date === '03' || date === '23') {
    date += 'rd';
  } else {
    date += 'th';
  }

  return month + ' ' + date + ' ' + year;
};

interface TodosProps {
  onDelete: (id: string) => void;
  deadline: string;
  title: string;
  id: string;
}

export const Todos: FC<TodosProps> = ({
  title,
  deadline,
  id,
  onDelete,
}): JSX.Element => {
  const formatDate = generateDate(deadline);
  const idTodo = id;

  return (
    <Todo>
      <div className="content-todo">
        <TodoTitle className="todo-title">{title}</TodoTitle>
        <TodoDeadline className="todo-deadline">{formatDate}</TodoDeadline>
      </div>
      <TrashIcon className="trash-icon" onClick={() => onDelete(idTodo)}>
        <BsTrash />
      </TrashIcon>
    </Todo>
  );
};
