import { FC } from 'react';
import { BsBookmarks } from 'react-icons/bs';

import { StyledHeader, ContentHeader, IconBookmark } from './StyledComponents';

export const HeaderTop: FC = (): JSX.Element => {
  return (
    <StyledHeader>
      <ContentHeader>
        <div className="title">Todo List</div>
        <IconBookmark>
          <BsBookmarks />
        </IconBookmark>
      </ContentHeader>
    </StyledHeader>
  );
};
