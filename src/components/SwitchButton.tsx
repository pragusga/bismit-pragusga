import { FC } from 'react';

import {
  InputCheck,
  SortName,
  SortDate,
  Label,
  Span,
} from './StyledComponents';

let sortByName = true;

export const SwitchButton: FC<{ setIsName: (setIsName: boolean) => void }> = ({
  setIsName,
}): JSX.Element => {
  const handleSwitch = (): void => {
    sortByName = !sortByName;
    setIsName(sortByName);
  };

  return (
    <Label>
      <SortName>Name</SortName>
      <SortDate>Date</SortDate>
      <InputCheck type="checkbox" onChange={handleSwitch} />
      <Span />
    </Label>
  );
};
