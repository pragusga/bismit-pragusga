import styled from 'styled-components';

// App
export const StyledApp = styled.div`
  font-family: 'IBM Plex Sans', sans-serif;
  height: 100%;
`;

export const ContentBody = styled.div`
  @media (max-width: 1820px) {
    width: 50%;
  }
  @media (max-width: 1506px) {
    width: 55%;
  }
  @media (max-width: 1460px) {
    width: 60%;
  }
  @media (max-width: 1256px) {
    width: 70%;
  }
  @media (max-width: 1075px) {
    width: 75%;
  }
  @media (max-width: 1040px) {
    width: 80%;
  }
  @media (max-width: 942px) {
    width: 90%;
  }
  width: 40%;
  margin: 0 auto 0 auto;
  overflow: hidden;
  padding-left: 2px;
`;

export const TitleTodo = styled.h2`
  color: #13293d;
  font-size: 30px;
  margin-top: 5px;
  margin-bottom: 20px;
  @media (max-width: 920px) {
    font-size: 25px;
  }
  @media (max-width: 386px) {
    font-size: 22px;
  }
`;

export const NewTask = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 20px;
  gap: 25px;
  font-size: 20px;
  font-weight: 600;
  @media (max-width: 781px) {
    display: flex;
    flex-direction: column;
  }
`;

export const SearchTask = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 649px) {
    flex-direction: column;
  }
`;
export const SortBy = styled.div`
  margin-top: 18px;
  display: flex;
  @media (max-width: 649px) {
    margin-top: -12px;
    margin-bottom: 25px;
  }
`;

export const InputSearch = styled.input`
  width: 500px;
  padding: 8px;
  @media (max-width: 920px) {
    width: 450px;
  }
  @media (max-width: 724px) {
    width: 380px;
  }
  @media (max-width: 464px) {
    width: 90%;
  }
`;

export const IconSearch = styled.div`
  position: relative;
  right: -92%;
  top: -25px;
  @media (max-width: 920px) {
    right: -420px;
  }
  @media (max-width: 724px) {
    right: -350px;
  }
  @media (max-width: 464px) {
    right: -80%;
  }
`;

export const StyledSearch = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  position: relative;
`;

export const TodoList = styled.div`
  position: relative;
  gap: 8px;
`;

export const Line = styled.hr`
  border-width: 2px;
  background-color: #13293d;
  margin-top: 20px;
  margin-bottom: 20px;
`;

// Filed New Task

export const TitleForm = styled.div`
  display: flex;
  justify-content: start;
  gap: 8px;
`;

export const TextForm = styled.div`
  font-size: 20px;
`;

export const IconStyled = styled.div`
  font-size: 18px;
  margin-top: 4px;
`;

export const InputDate = styled.input`
  padding: 6px 8px 6px 8px;
  &:hover {
    cursor: pointer;
  }
`;

export const InputActivity = styled.input`
  padding: 8px;
  width: 400px;
  @media (max-width: 928px) {
    width: 350px;
  }
  @media (max-width: 780px) {
    width: 380px;
  }
  @media (max-width: 464px) {
    width: 90%;
  }
`;

export const StyledBtnSubmit = styled.button`
  padding: 1px 20px 1px 20px;
  background-color: white;
  height: 37px;
  cursor: pointer;
  margin-top: 25px;
  border-radius: 5px;
  font-weight: 600;
  &:hover {
    background-color: #ebebeb;
  }
  @media (max-width: 781px) {
    width: 120px;
    margin-top: 5px;
  }
`;

// Header Top

export const StyledHeader = styled.div`
  height: 150px;
  background-color: #13293d;
  color: white;
  font-size: 80px;
  padding-top: 0.1px;
  @media (max-width: 616px) {
    font-size: 60px;
  }
  @media (max-width: 386px) {
    font-size: 50px;
    height: 120px;
  }
`;

export const ContentHeader = styled.div`
  @media (max-width: 1820px) {
    width: 50%;
  }
  @media (max-width: 1506px) {
    width: 55%;
  }
  @media (max-width: 1460px) {
    width: 60%;
  }
  @media (max-width: 1256px) {
    width: 70%;
  }
  @media (max-width: 1075px) {
    width: 75%;
  }
  @media (max-width: 1040px) {
    width: 80%;
  }
  @media (max-width: 942px) {
    width: 90%;
  }

  width: 40%;
  margin: 50px auto 0 auto;
  display: flex;

  @media (max-width: 386px) {
    margin: 30px auto 0 auto;
  }
`;

export const IconBookmark = styled.div`
  margin-top: 10px;
  margin-left: 20px;
`;

// Switch Button

export const Label = styled.label`
  margin-left: 10px;
  position: relative;
  display: inline-block;
  width: 110px;
  height: 32px;
  background-color: #dedede;
  border-radius: 10px;
  padding-left: 2px;
  padding-right: 2px;
  box-shadow: 1px 1px 2px -2px #9e9d99 inset, -1px -1px 2px 1px #9e9d99 inset;
  cursor: pointer;
`;

export const Span = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  &:before {
    text-align: center;
    position: absolute;
    content: '';
    border-radius: 8px;
    padding-right: 10px;
    height: 26px;
    width: 50px;
    background-color: #fafafa;
    box-shadow: 2px 1px 5px 2px #969696;
    transition: 0.4s;
    top: 2.95px;
    left: 4px;
  }
`;
export const InputCheck = styled.input`
  opacity: 0;
  width: 0;
  height: 0;

  &:checked + ${Span}:before {
    padding-right: 0;
    transform: translateX(55px);
  }
`;

export const SortName = styled.div`
  position: absolute;
  font-weight: 500;
  z-index: 999;
  top: 5px;
  left: 15px;
  font-size: 14px;
`;

export const SortDate = styled.div`
  position: absolute;
  font-weight: 500;
  z-index: 999;
  top: 5px;
  right: 15px;
  font-size: 14px;
`;

// Text Box
export const StyledTextBox = styled.div`
  padding: 7px 13px 7px 13px;
  border-radius: 5px;
  background-color: #13293d;
  color: white;
  font-weight: 600;
  font-size: 18px;
  width: max-content;
`;

// Todo
export const Todo = styled.div`
  padding: 15px;
  background-color: white;
  width: 100%;
  border-radius: 5px;
  margin-bottom: 12px;
  display: flex;
  justify-content: space-between;
`;

export const TodoTitle = styled.div`
  color: #13293d;
  font-weight: 600;
`;

export const TodoDeadline = styled.div`
  font-size: 12px;
  margin-top: 4px;
`;

export const TrashIcon = styled.div`
  transform: translate(-80%, 10%);
  font-size: 30px;
  cursor: pointer;
  &:hover {
    color: grey;
  }
`;
