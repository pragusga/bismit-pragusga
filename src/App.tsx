import { SwitchButton } from './components/SwitchButton';
import { FieldNewTask } from './components/FieldNewTask';
import { HeaderTop } from './components/HeaderTop';
import { FC, useEffect, useState } from 'react';
import { TextBox } from './components/TextBox';
import { Todos } from './components/Todo';
import { BiSearch } from 'react-icons/bi';
import { v4 } from 'uuid';

import {
  StyledSearch,
  InputSearch,
  ContentBody,
  SearchTask,
  IconSearch,
  TitleTodo,
  StyledApp,
  TodoList,
  NewTask,
  SortBy,
  Line,
} from './components/StyledComponents';

interface Data {
  titleTask: string;
  deadlineDate: string;
  id: string;
}

const localData: Data[] = JSON.parse(localStorage.getItem('data') || '[]');

const App: FC = () => {
  const [data, setData] = useState<Data[]>(localData);
  const [isName, setIsName] = useState<boolean>(true);
  const [search, setSearch] = useState<string>('');
  let datas: Data[];
  if (search !== '' || search !== null) {
    datas = data.filter((d) => {
      return (
        d.titleTask.toLowerCase().indexOf(search.toLocaleLowerCase()) !== -1
      );
    });
  } else {
    datas = [...data];
  }

  const submitData = (titleTask: string, deadlineDate: string): void => {
    const newData = [...data, { titleTask, deadlineDate, id: v4() }];
    setData([...newData]);
  };

  const handleDelete = (id: string): void => {
    const dataAfterDelete = data.filter((d) => d.id !== id);
    setData([...dataAfterDelete]);
  };

  useEffect(() => {
    const someData = [...data];
    localStorage.setItem('data', JSON.stringify(someData));
  }, [data]);

  return (
    <StyledApp>
      <HeaderTop />
      <ContentBody>
        <TitleTodo>Organize Your Task Better</TitleTodo>
        <TextBox>New Task</TextBox>
        <NewTask>
          <FieldNewTask inputData={submitData} />
        </NewTask>
        <Line />
        <TextBox>Task List</TextBox>
        <SearchTask>
          <StyledSearch>
            <InputSearch
              type="search"
              placeholder="Seacrh activities..."
              onChange={(e) => setSearch(e.target.value)}
            />
            <IconSearch>
              <BiSearch />
            </IconSearch>
          </StyledSearch>
          <SortBy>
            <div style={{ marginTop: '4px' }}>
              <span>Sort By</span>
            </div>
            <SwitchButton setIsName={setIsName} />
          </SortBy>
        </SearchTask>
        <TodoList>
          {data
            ? isName
              ? datas
                  .sort((prev, next) => {
                    if (
                      prev.titleTask.toLowerCase() <
                      next.titleTask.toLowerCase()
                    )
                      return -1;
                    if (
                      prev.titleTask.toLowerCase() >
                      next.titleTask.toLowerCase()
                    )
                      return 1;
                    return 0;
                  })
                  .map((d, index) => {
                    return (
                      <Todos
                        title={d.titleTask}
                        deadline={d.deadlineDate}
                        id={d.id}
                        onDelete={handleDelete}
                        key={index}
                      />
                    );
                  })
              : datas
                  .sort((prev, next) => {
                    if (prev.deadlineDate < next.deadlineDate) return -1;
                    if (prev.deadlineDate > next.deadlineDate) return 1;
                    return 0;
                  })
                  .map((d, index) => {
                    return (
                      <Todos
                        title={d.titleTask}
                        deadline={d.deadlineDate}
                        id={d.id}
                        onDelete={handleDelete}
                        key={index}
                      />
                    );
                  })
            : null}
        </TodoList>
      </ContentBody>
    </StyledApp>
  );
};

export default App;
